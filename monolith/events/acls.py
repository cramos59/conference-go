from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_weather_data(conference):
    city_name = conference.location.city
    state_code = conference.location.state.abbreviation

    geo_response = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q={city_name},{state_code}&limit=1&appid={OPEN_WEATHER_API_KEY}"
        )

    geocoding = json.loads(geo_response.content)

    print("Geocoding Response:", geocoding)

    if len(geocoding) > 0:
        geo_info = geocoding[0]
        lat = geo_info["lat"]
        lon = geo_info["lon"]

    if len(geocoding) == 0:
        return None

    weather_response = requests.get(
        f"http://api.openweathermap.org/geo/1.0/reverse?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    )
    weather_data = json.loads(weather_response.content)

    print("Weather Response:", weather_data)

    weather = {
        "temp": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
    return weather


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state,
        "per_page": 1
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
